class Usuarios {
    constructor() {
        this.personas = [];
    }

    agregarPersonas(id, nombre, sala) {
        let persona = { id, nombre , sala};
        const encontradoPorNombre = this.personas.find(
            (element) => element.nombre == nombre
        );
        const encontradoPorId = this.personas.find(
            (element) => element.id == id
        );
        if (encontradoPorNombre === undefined && encontradoPorId === undefined) { // TODO: tal ves se puede poner solo una condición en este if
            this.personas.push(persona);
            return this.personas;
        } else {
            return this.personas;
        }
    }

    getPersona(id) {
        let persona = this.personas.filter((persona) => persona.id === id)[0];

        return persona;
    }

    getPersonas() {
        return this.personas;
    }

    getPersonasPorSala(sala){
        let personasEnSala = this.personas.filter(
            (persona) => persona.sala == sala
        );
        return personasEnSala;
    }


    borrarPersona(id) {
        let personaBorrada = this.getPersona(id);
        // console.log("personas: ", this.personas);
        // console.log("por borrar: ", personaBorrada);
        if (personaBorrada === undefined) {
            return false;
        } else {
            this.personas = this.personas.filter(
                (persona) => persona.id !== id
            );
            return personaBorrada;
        }
    }
}

module.exports = {
    Usuarios,
};
